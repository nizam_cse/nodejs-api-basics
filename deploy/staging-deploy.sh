#!/bin/bash

# any future command that fails will exit the script
set -e

cd ~
echo "ALL_SERVERS FROM UPDATE ${ALL_SERVERS}"

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install node

npm remove pm2 -g

npm install pm2 -g

pm2 status

# Delete the old repo
sudo rm -rf nodejs-api-basics

# Delete OLD DEV
sudo rm -rf study-camp-stage-api

# clone the repo again
git clone https://gitlab.com/nizam_cse/nodejs-api-basics.git

sudo mv nodejs-api-basics study-camp-stage-api
cd study-camp-stage-api
git checkout staging
git pull

#install npm packages
echo "Running npm install"
npm install

#Restart the node server
echo "pm2 deleting stage server"
pm2 delete -s development || :
echo "PM2 starting ..."
echo "pm2 start stage-server.js"
pm2 start "stage-server.js" --name=development