#!/bin/bash

# any future command that fails will exit the script
set -e

cd ~
echo "ALL_SERVERS FROM UPDATE ${ALL_SERVERS}"

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install node

npm remove pm2 -g

npm install pm2 -g

pm2 status

# Delete the old repo
rm -rf nodejs-api-basics

# clone the repo again
git clone https://gitlab.com/nizam_cse/nodejs-api-basics.git

#mv nodejs-api-basics node-api
cd nodejs-api-basics

git checkout development
git pull

#install npm packages
echo "Running npm install"
npm install

#Restart the node server

echo "PM2 starting ..."
pm2 restart server.js