#!/bin/bash
# any future command that fails will exit the script
set -e
#./deploy/disableHostKeyChecking.sh

# Lets write the public key of our aws instance
eval $(ssh-agent -s)
#echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
bash ./deploy/disableHostKeyChecking.sh

# ** Alternative approach
echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.


# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVERS=$DEPLOY_SERVERS
echo "Hello ${PRIVATE_KEY} ${DEPLOY_SERVERS}"
echo "${CI_COMMIT_BRANCH} branch ${CI_COMMIT_BRANCH} rf ${CI_COMMIT_REF}"
# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

echo "${CI_COMMIT_TAG} and ${CI_COMMIT_BRANCH}"

# Lets iterate over this array and ssh into each EC2 instance
# Once inside the server, run updateAndRestart.sh
for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  if [ "${CI_COMMIT_BRANCH}" = "development" ]
  then
    echo "Development Deployment Starting"
    ssh -i "${PRIVATE_KEY}" ec2-user@ec2-3-6-41-166.ap-south-1.compute.amazonaws.com 'bash' < "./deploy/development-deploy.sh"
  elif [ "${CI_COMMIT_BRANCH}" = "staging" ]
  then
    echo "Staging Deployment Starting"
    ssh -i "${PRIVATE_KEY}" ec2-user@ec2-3-6-41-166.ap-south-1.compute.amazonaws.com 'bash' < "./deploy/staging-deploy.sh"
  elif [ "${CI_COMMIT_BRANCH}" = "master" ]
  then
    echo "Production Deployment Starting"
    ssh -i "${PRIVATE_KEY}" ec2-user@ec2-3-6-41-166.ap-south-1.compute.amazonaws.com 'bash' < "./deploy/production-deploy.sh"
  fi
done
