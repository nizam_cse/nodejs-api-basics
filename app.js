const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const productRoutes = require("./api/routes/products");
const ordertRoutes = require("./api/routes/orders");
const userRoutes = require("./api/routes/users");
const categoryRoutes = require("./api/routes/categories");
const subjectRoutes = require("./api/routes/subjects");
app.use(morgan("dev"));
app.use(express.static("public"));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method == "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE");
    return res.status(200).json({});
  }
  next();
});

app.use("/", userRoutes);

app.use("/products", productRoutes);
app.use("/orders", ordertRoutes);
app.use("/users", userRoutes);
app.use("/categories", categoryRoutes);
app.use("/subjects", subjectRoutes);

app.use((req, resp, next) => {
  const error = new Error("Not Found");
  error.status = 404;
  next(error);
});

app.use((error, req, resp, next) => {
  resp.status(error.status || 500);
  resp.json({
    error: {
      message: error.message,
    },
  });
});
module.exports = app;
