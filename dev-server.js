const http = require('http')
const mongoose = require('mongoose')
try {
  mongoose.connect(process.env.MONGO_ATLAS_DEV_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  console.log('Successfully connected Mongo Atlas Dev')
} catch (e) {
  console.log(
    'Mongo Atlas Stage connection error:',
    e,
    'MONGO_ATLAS_STAGE_URL: ' + process.env.MONGO_ATLAS_DEV_URL
  )
}
const app = require('./app')
const port = process.env.DEV_PORT || 4800
const server = http.createServer(app)
server.listen(port, () => {
  console.log(`Server started to: http://localhost:${port}/`)
})
