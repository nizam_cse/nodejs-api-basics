const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const saltRounds = 10
const jwt = require('jsonwebtoken')
const User = require('../models/user')
const { token } = require('morgan')

router.get('/', (req, res, next) => {
  User.find()
    .exec()
    .then((users) => {
      return res
        .status(200)
        .json([{ users: users, env: 'production server updated' }])
    })
})

router.post('/signup', (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then((user) => {
      if (user.length > 0) {
        return res.status(409).json({
          message: 'User already exist'
        })
      } else {
        bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
          if (err) {
            console.log(err)
            return res.status(500).json({
              error: err
            })
          } else {
            const user = new User({
              _id: mongoose.Types.ObjectId(),
              name: req.body.name,
              email: req.body.email,
              password: hash
            })
            user
              .save()
              .then((result) => {
                console.log(result)
                res.status(201).json({
                  message: 'Successfully created user.'
                })
              })
              .catch((err) => {
                return res.status(500).json({
                  error: err
                })
              })
          }
        })
      }
    })
    .catch((err) => {
      return res.status(500).json({
        error: err
      })
    })
})

router.post('/login', (req, res, next) => {
  User.find({ email: req.body.email })
    .then((user) => {
      if (user.length < 1) {
        return res.status(404).json({
          message: 'User does not exist.'
        })
      } else {
        bcrypt.compare(req.body.password, user[0].password, function (
          err,
          result
        ) {
          if (err) {
            return res.status(401).json({
              error: "Credential doesn't match"
            })
          }
          if (result) {
            let token = jwt.sign(
              {
                name: user[0].name,
                email: user[0].email,
                _id: user[0]._id
              },
              process.env.JWT_SECRET,
              {
                expiresIn: 60 * 60
              }
            )
            return res.status(200).json({
              message: 'Login successfull',
              token: token
            })
          }
          return res.status(401).json({
            error: "Credential doesn't match"
          })
        })
      }
    })
    .catch((err) => {
      return res.status(500).json({
        error: err
      })
    })
})

router.delete('/:userId', (req, res, next) => {
  User.remove({ _id: req.params.id })
    .exec()
    .then((result) => {
      return res.status(200).json({
        message: 'User has been deleted'
      })
    })
    .catch((err) => {
      return res.status(500).json({
        error: err
      })
    })
})

module.exports = router

// openssl rand -base64 64
