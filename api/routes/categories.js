const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Category = require("../models/category");

router.get("/", (req, res, next) => {
  Category.find()
    .exec()
    .then((categories) => {
      res.status(201).json(categories);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.post("/", (req, res, next) => {
  const category = new Category({
    _id: mongoose.Types.ObjectId(),
    name: req.body.name,
  });
  category
    .save()
    .then((category) => {
      res.status(201).json(category);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  Category.findById(id)
    .exec()
    .then((category) => {
      if (category) res.status(200).json(category);
      else {
        res.status(404).json({ message: "Category not found." });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.patch("/:id", (req, res, next) => {
  const id = req.params.id;
  const newProps = {
    name: req.body.name,
  };
  Category.updateOne({ _id: id }, { $set: newProps })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  Category.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "Category deleted successfully",
        request: {
          type: "POST",
          url: "http://localhost:5000/categories",
          data: { name: "String" },
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;
