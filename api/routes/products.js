const express = require("express");
const router = express.Router();

const Product = require("../models/product");
const mongoose = require("mongoose");
const multer = require("multer");

const auth = require("../middleware/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/images/");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString() + file.originalname);
  },
});
const filterFile = (req, file, cb) => {
  if (["image/jpeg", "image/jpg", "image/png"].includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 1,
  },
  fileFilter: filterFile,
});

router.get("/", (req, res, next) => {
  Product.find()
    .exec()
    .then((products) => {
      const response = {
        count: products.length,
        products: products.map((p) => {
          return {
            _id: p._id,
            name: p.name,
            price: p.price,
            image: p.image,
            request: {
              type: "GET",
              url: `http://localhost:5000/products/${p._id}`,
            },
          };
        }),
      };
      res.status(200).json(response);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.post("/", auth, upload.single("productImage"), (req, res, next) => {
  const product = new Product({
    _id: mongoose.Types.ObjectId(),
    name: req.body.name,
    price: req.body.price,
    image: req.file.path,
  });

  product
    .save()
    .then((result) => {
      res.status(201).json({
        message: "Product created successfully",
        product: {
          _id: product._id,
          name: product.name,
          price: product.price,
          image: product.image,
        },
        request: {
          type: "GET",
          url: `http://localhost:5000/products/${product._id}`,
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  Product.findById(id)
    .exec()
    .then((product) => {
      if (product)
        res.status(200).json({
          product: {
            _id: product._id,
            name: product.name,
            price: product.price,
            image: product.image,
          },
          request: {
            type: "GET",
            url: `http://localhost:5000/products`,
          },
        });
      else {
        res.status(404).json({ message: "Product not found." });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.patch("/:id", upload.single("productImage"), (req, res, next) => {
  const id = req.params.id;
  const newProps = {
    name: req.body.name,
    price: req.body.price,
    image: req.file.path,
  };
  Product.updateOne({ _id: id }, { $set: newProps })
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "Product updated successfully",
        updatedProps: newProps,
        request: {
          type: "GET",
          url: `http://localhost:5000/products/${id}`,
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  Product.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "Product deleted successfully",
        request: {
          type: "POST",
          url: "http://localhost:5000/products",
          data: { name: "String", price: "Number" },
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;
