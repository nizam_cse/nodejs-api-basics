const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Subject = require("../models/subject");

router.get("/", (req, res, next) => {
  Subject.find()
    .exec()
    .then((subjects) => {
      res.status(201).json(subjects);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.post("/", (req, res, next) => {
  const subject = new Subject({
    _id: mongoose.Types.ObjectId(),
    name: req.body.name,
  });
  subject
    .save()
    .then((subject) => {
      res.status(201).json(subject);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  Subject.findById(id)
    .exec()
    .then((subject) => {
      if (subject) res.status(200).json(subject);
      else {
        res.status(404).json({ message: "Subject not found." });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.patch("/:id", (req, res, next) => {
  const id = req.params.id;
  const newProps = {
    name: req.body.name,
  };
  Subject.updateOne({ _id: id }, { $set: newProps })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  Subject.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "Subject deleted successfully",
        request: {
          type: "POST",
          url: "http://localhost:5000/subjects",
          data: { name: "String" },
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;
