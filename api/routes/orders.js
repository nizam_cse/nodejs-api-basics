const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Order = require("../models/order");
const Product = require("../models/product");

router.get("/", (req, res, next) => {
  Order.find()
    .populate("product", "_id name price")
    .exec()
    .then((orders) => {
      const response = {
        count: orders.length,
        orders: orders.map((order) => {
          return {
            _id: order._id,
            product: order.product,
            quantity: order.quantity,
            request: {
              type: "GET",
              url: `http://localhost:5000/orders/${order._id}`,
            },
          };
        }),
      };
      res.status(201).json(response);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.post("/", (req, res, next) => {
  Product.findById(req.body.productId)
    .then((product) => {
      if (!product)
        return res.status(404).json({ message: "Product not found." });
      const order = new Order({
        _id: mongoose.Types.ObjectId(),
        product: product._id,
        quantity: req.body.quantity,
      });
      return order.save();
    })
    .then((result) => {
      res.status(201).json({
        message: "Order created successfully",
        order: {
          _id: result.id,
          product: result.product,
          quantity: result.quantity,
        },
        request: {
          type: "GET",
          url: `http://localhost:5000/orders/${result._id}`,
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
});

router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  Order.findById(id)
    .exec()
    .then((order) => {
      if (order)
        res.status(200).json({
          order: {
            _id: order._id,
            product: order.product,
            quantity: order.quantity,
          },
          request: {
            type: "GET",
            url: `http://localhost:5000/orders`,
          },
        });
      else {
        res.status(404).json({ message: "Order not found." });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

router.patch("/:id", (req, res, next) => {
  const id = req.params.id;
  res.status(200).json({
    message: "Hello PATCH request order " + id,
  });
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  Order.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json({
        message: "Order deleted successfully",
        request: {
          type: "POST",
          url: "http://localhost:5000/orders",
          data: { productId: "ID", quantity: "Number" },
        },
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;
